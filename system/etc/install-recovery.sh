#!/system/bin/sh
  echo 1 > /sys/module/sec/parameters/recovery_done		#tony
if ! applypatch -c EMMC:recovery:8347648:49d5ab9bbd311aca6a89e9d78c2a78d4d4c245fa; then
  log -t recovery "Installing new recovery image"
  applypatch -b /system/etc/recovery-resource.dat EMMC:boot:5873664:eafe275a943360b586f43cc900efd61ae66428ef EMMC:recovery 49d5ab9bbd311aca6a89e9d78c2a78d4d4c245fa 8347648 eafe275a943360b586f43cc900efd61ae66428ef:/system/recovery-from-boot.p
  if applypatch -c EMMC:recovery:8347648:49d5ab9bbd311aca6a89e9d78c2a78d4d4c245fa; then		#tony
	echo 0 > /sys/module/sec/parameters/recovery_done		#tony
        log -t recovery "Install new recovery image completed"
  else
	echo 2 > /sys/module/sec/parameters/recovery_done		#tony
        log -t recovery "Install new recovery image not completed"
  fi
else
  echo 0 > /sys/module/sec/parameters/recovery_done              #tony
  log -t recovery "Recovery image already installed"
fi
