#!/system/bin/sh
#Jevons@OnLineRD.DeviceService, 2013/10/23 add this file for properties

#ifdef VENDOR_EDIT
#wangjimin@EXP.SysFramework.build,add for Singapore Taiwan operator,20141022
#Qiang.shao@EXP.SysFramework.build,add for DZ, 20150213
#  Value      Operator
#  00000000   EX
#  00000001   SINGTEL
#  00000010   STARHUB
#  00000011   M1
#  00000100   CHT
#  00000101   FET
#  00000110   TWM
#  00000111   VBO
#  00001000   VN (region)
#  00001001   TWOP    all Taiwan Operators
#  00001010   DZ

#no need use the property in MX build,because it compile independently
mx_or_us=` getprop persist.sys.oppo.region`

if [ $mx_or_us = "MX" ] ; then
    setprop persist.sys.oppo.region MX
else
    exp_operator=`cat /data/nvram/APCFG/APRDEB/CARRIER_VER`

    #Qiang.shao@EXP.SysFramework.build, add for remove tail, because MTK NV endswith "0xAA 0x00" when we read data directly ( MTK self-check mechanism ), 20150318
    exp_operator=${exp_operator:0:8}

    if [ $exp_operator = "00000001" ] ; then
       setprop ro.oppo.operator SINGTEL
       setprop persist.sys.oppo.region SG
       #display_id=`getprop ro.build.display.id`
       #display_id_op=$display_id"_SINGTEL"
       #setprop ro.build.display.id $display_id_op
    elif [ $exp_operator = "00000010" ] ; then
       setprop ro.oppo.operator STARHUB
       setprop persist.sys.oppo.region SG
       #display_id=`getprop ro.build.display.id`
       #display_id_op=$display_id"_STARHUB"
       #setprop ro.build.display.id $display_id_op
    elif [ $exp_operator = "00000011" ] ; then
       setprop ro.oppo.operator M1
       setprop persist.sys.oppo.region SG
    elif [ $exp_operator = "00000100" ] ; then
       setprop ro.oppo.operator CHT
       setprop persist.sys.oppo.region TW
       #display_id=`getprop ro.build.display.id`
       #display_id_op=$display_id"_STARHUB"
       #setprop ro.build.display.id $display_id_op
    elif [ $exp_operator = "00000101" ] ; then
       setprop ro.oppo.operator FET
       setprop persist.sys.oppo.region TW
    elif [ $exp_operator = "00000110" ] ; then
       setprop ro.oppo.operator TWM
       setprop persist.sys.oppo.region TW
    elif [ $exp_operator = "00000111" ] ; then
       setprop ro.oppo.operator VBO
       setprop persist.sys.oppo.region TW
    #no need set VN, just delete wechat by EngineerMode,20150205
    #elif [ $exp_operator = "00001000" ] ; then
    #   setprop persist.sys.oppo.region VN
    elif [ $exp_operator = "00001001" ] ; then
       setprop ro.oppo.operator TWOP
       setprop persist.sys.oppo.region TW
    elif [ $exp_operator = "00001010" ] ; then
       setprop ro.oppo.specialdemand DZ
    fi
fi
#endif /* VENDOR_EDIT */

