#!/system/bin/sh
config="$1"

case "$config" in
	"performance")
		echo 1 > /sys/devices/system/cpu/cpu2/online
		echo 1 > /sys/devices/system/cpu/cpu3/online
		echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
		;;
	"hotplug")
		echo 0 > /sys/devices/system/cpu/cpu2/online
		echo 0 > /sys/devices/system/cpu/cpu3/online
        echo hotplug > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
		;;			
     *)
      ;; 
esac


